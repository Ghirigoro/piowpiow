﻿using UnityEngine;

namespace gc {

	public class Game : MonoBehaviour {

		public int MinNumEnemies = 4;

		void Update () {
			// Spawn player
			if (Player.Instance == null) {
				Spawner.Spawn("Prefabs/Playership", RandomWorldPosition(3));	
			}

			// Spawn enemies
			if (Enemy.NumEnemies < MinNumEnemies) {
				Spawner.Spawn("Prefabs/Enemyship", RandomWorldPosition(3));
			}

		}
			
		private Vector3 RandomWorldPosition(int gutter) {
			return new Vector3(Random.Range(gutter, Config.WorldWidth - gutter), Random.Range(gutter, Config.WorldHeight - gutter), 0); 
		}

	}

}

