﻿using UnityEngine;

namespace gc {

public class Config : MonoBehaviour
	{
	    public static readonly int ScreenWidth = 1200;
	    public static readonly int ScreenHeight = 900;

		public static readonly int SpriteScale = 10;

		public static readonly int WorldWidth = ScreenWidth / SpriteScale;
		public static readonly int WorldHeight = ScreenHeight / SpriteScale;
	}
}
