﻿using UnityEngine;

namespace gc {

    public abstract class MovementController : MonoBehaviour {

		protected Rigidbody2D body;

        void Awake () {
            body = GetComponent<Rigidbody2D>();
            if (body == null) {
                throw new MissingComponentException("MovementController added to game object that has no Rigidbody2D");
            }
        }

        abstract protected void FixedUpdate();
    }
}
